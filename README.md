# nodejs-roadmap

## Workshops

- 1 => []
- 2 => []
- 3 => []
- 4 => []
- 5 => []
- 6 => []

## Phase 1

**Title**: Nodejs HTTP server + User CRUD

**Description**: Implement a basic Nodejs CRUD app using `vanillaNodejs`. Using `FileSystem` as data source.

**Functionalities**:

- Add new user
- Update specific user
- Delete specific user
- Get specific user detail
- List all users

**Important tips in development:**

- Analyze User entity & its fields (`username/password/...`)
- Proper validation for all fields
- Basic Error handling
- Proper routes/paths/query-params...
- Proper `File` operations (`open/close/write/append/read/...`)

<br />

## Phase 2

**Title**: URL shortener

**Description**: Add all CRUD functionalities to the previous section (User) for `URL` entity. Using `FileSystem` as data source.

**Functionalities**:

- Add new url
- Update specific url
- Delete specific url
- Get specific url detail
- List all urls

**Important tips in development:**

- Analyze URL entity & URL + User fields
- Proper validation for all fields
- Basic Error handling
- Proper routes/paths/query-params...
- Proper `File` operations (`open/close/write/append/read/...`)
- **Use Nodejs** `Module System` & **Refactor** all previous functionalities

<br />

## Phase 3

**Title**: Add Development Tools + Asynchronous

**Description**: Install & Setup `ESLint/Prettier/Docker` + Refactor all i/o operations to be Asynchronous (`Promise or async/await`)

**Docker**

- Install `Docker` & `Compose (docker compose)`
- Create container file for app `Dockerfile`
- Create compose config file for all services `docker-compose.yml`

**ESLint**

- Install `eslint` using yarn or npm
- Add proper eslint `plugins` & `extends` (`sonarjs/airbnb/prettier`)

**Prettier**

- Install vscode prettier extension
- Add `.prettierrc` & `.prettierignore`

<br />

## Phase 4

**Title**: Add **MongoDB & Mongoose**

**Description**: Use `MongoDB` & `Mongoose` instead of FileSystem for `User`

**Functionalities**:

- Install & Add `MongoDB` service by **Docker**
- Install `Mongoose`
- Connect to `MongoDB`
- Create User DataModel using Mongoose
- Refactor all storage operations to use `database` instead of `file`

<br />

## Phase 5

**Title**: Add **Redis**

**Description**: Use `Redis` instead of FileSystem for `URL`

**Functionalities**:

- Install & Add `Redis` service by **Docker**
- Install proper library (`ioredis/...`)
- Connect to `Redis`
- Refactor all storage operations to use `redis` instead of `file`

<br />

## Phase 6

**Title**: Add Test

**Description**: Add `Unit & Integration` tests to all functionalities and operations. Choose library yourself.

## Phase 7

**Title**: Add Fastify

**Description**: Move from `VanillaNodejs` to `Fastify` framework
